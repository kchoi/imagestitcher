#include "backend.h"
#include <KJobUiDelegate>
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QImage>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/stitching.hpp>
#include <opencv2/core/cvstd.hpp>

QImage cv2qimage(cv::Mat mat)
{
    if (mat.data == NULL) {
        return QImage();
    }
    cv::Mat inversed;
    cv::cvtColor(mat, inversed, cv::COLOR_BGR2RGB);
    QImage image = QImage((uchar*)inversed.data, inversed.cols,
      inversed.rows, inversed.step, QImage::Format_RGB888).copy();
    return image;
}

Backend::Backend(QObject *parent)
    : QAbstractListModel(parent)
{

}

void Backend::stitch()
{
    qDebug() << "Doing the stitch";
    cv::Ptr<cv::Stitcher> stitcher = cv::Stitcher::create(cv::Stitcher::SCANS);
    qDebug() << this->images.size();
    cv::Stitcher::Status status = stitcher->stitch(this->images, this->m_stitched);
    if (status == cv::Stitcher::ERR_NEED_MORE_IMGS)
    {
        qDebug() << "Can't stitch images, error code = " << int(status);
        this->m_stitched.release();
    } else {
        qDebug() << "Stitching done" << this->m_stitched.total();
    }
    emit stitchedChanged();
}

void Backend::downloadFinished(KJob *job)
{
    if (job->error()) {
        qDebug() << job->errorString();
        job->uiDelegate()->showErrorMessage();
        return;
    }
    
    const KIO::StoredTransferJob *storedJob = qobject_cast<KIO::StoredTransferJob *>(job);
    
    if (storedJob) {
        cv::Mat image;
        // buffer mat has to be inline in imdecode ! I don't know why...
        image = cv::imdecode(cv::Mat(1, storedJob->data().length(),
                                     CV_8UC1, storedJob->data().data()),
                             cv::ImreadModes::IMREAD_COLOR);
        if (image.data == NULL) {
            qDebug() << "failed to decode " << storedJob->url();
        } else {
            beginInsertRows(QModelIndex(), this->urls.size(), this->urls.size());
            this->images.push_back(image);
            this->urls.push_back(storedJob->url());
            endInsertRows();
            qDebug() << "Backend inserted " << this->urls.at(this->urls.size() - 1);
            emit dataChanged(index(this->urls.size() - 1), index(this->urls.size() - 1));
        }
    }
}

QImage Backend::stitched() const
{
    return cv2qimage(this->m_stitched);
}

int Backend::rowCount(const QModelIndex &) const {
    qDebug() << "rowCount " << this->urls.count();
    return this->urls.count();
}

QHash<int, QByteArray> Backend::roleNames() const {
    qDebug() << "roleNames";
    return {
        {UrlRole,   "url"},
        {ImageRole, "image"}
    };
}

QVariant Backend::data(const QModelIndex &index, int role) const {
    qDebug() << "data role " << role;
    switch (role) {
        case UrlRole:
            qDebug() << "data url " << this->urls.at(index.row());
            return this->urls.at(index.row());
        case ImageRole:
            return cv2qimage(this->images.at(index.row()));
        default:
            return QVariant();
    }
}

void Backend::addImage(const QUrl& url)
{
    KIO::Job *job = KIO::storedGet(url);
    connect(job, &KJob::result, this, &Backend::downloadFinished);
    job->exec();
}

void Backend::removeImage(int rowIndex)
{
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    this->images.erase(this->images.begin()+rowIndex);
    this->urls.remove(rowIndex);
    endRemoveRows();
    emit dataChanged(index(rowIndex), index(rowIndex));
}

void Backend::removeAllImages()
{
    beginResetModel();
    this->images.clear();
    this->urls.clear();
    endResetModel();
}

void Backend::save(const QUrl& url)
{
    const std::string ext = ("." + QFileInfo(url.fileName()).suffix()).toStdString();
    std::vector<uchar> buffer;
    cv::imencode(ext, this->m_stitched, buffer);
    KIO::Job *job = KIO::storedPut(QByteArray(reinterpret_cast<const char*>(buffer.data()), buffer.size()), url, -1, KIO::Overwrite);
    connect(job, &KJob::result, this, &Backend::uploadFinished);
    job->exec();
}

void Backend::uploadFinished(KJob *job)
{
    if (job->error()) {
        qDebug() << job->errorString();
        job->uiDelegate()->showErrorMessage();
        return;
    }
    Q_EMIT saveFinished();
}

void Backend::clearStitched()
{
    this->m_stitched.release();
    emit stitchedChanged();
}
