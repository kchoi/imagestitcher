import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import QtQml
import QtQuick.Dialogs
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kquickimageeditor 1.0 as KQuickImageEditor

Kirigami.ScrollablePage {
    title: i18nc("@title", "Image List")

    signal stitch()

    actions: [
        Kirigami.Action {
            icon.name: "list-add"
            text: i18nc("@action:button", "Add Images")
            shortcut: "Ctrl+o"
            onTriggered: openFileDialog.open()
        },
        Kirigami.Action {
            icon.name: "delete"
            text: i18nc("@action:button", "Remove all images")
            onTriggered: backend.removeAllImages()
            enabled: imageList.count >= 1
        },
        Kirigami.Action {
            id: stitchAction
            icon.name: "media-playback-start"
            text: i18nc("@action:button", "Stitch")
            enabled: imageList.count >= 2
            onTriggered: {
                backend.stitch()
                if (pageStack.get(1) == null) {
                    pageStack.push(resultPage)
                }
            }
        }
    ]

    FileDialog {
        id: openFileDialog
        fileMode: FileDialog.OpenFiles
        //currentFolder: StandardPaths.standardLocations(StandardPaths.PicturesLocation)[0]
        currentFolder: "file:/home/kwon-young/Documents/peoples/Papa/"
        selectedNameFilter.index: 0
        nameFilters: ["All Supported Formats (*.jpg *.bmp *.dib *.jpeg *.jpe *.jp2 *.png *.webp *.avif *.pbm *.pgm *.ppm *.pxm *.pnm *.pfm *.sr *.ras *.tiff *.tif *.exr *.hdr *.pic)",
                      "Windows bitmaps (*.bmp *.dib)",
                      "JPEG files (*.jpeg *.jpg *.jpe)",
                      "JPEG 2000 files (*.jp2)",
                      "Portable Network Graphics (*.png)",
                      "WebP (*.webp)",
                      "AVIF (*.avif)",
                      "Portable image format (*.pbm *.pgm *.ppm *.pxm *.pnm)",
                      "PFM files (*.pfm)",
                      "Sun rasters (*.sr *.ras)",
                      "TIFF files (*.tiff *.tif)",
                      "OpenEXR Image files (*.exr)",
                      "Radiance HDR (*.hdr *.pic)"]
        onAccepted: {
            for (var i=0; i<selectedFiles.length; i++) {
                backend.addImage(selectedFiles[i])
            }
        }
    }

    ListView {
        id: imageList
        model: backend

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            width: parent.width - Kirigami.Units.largeSpacing * 4
            visible: imageList.count === 0
            text: "Welcome to the ImageStitcher program !\nStart by selecting images to stitch together by clicking on the button below"

            helpfulAction: Kirigami.Action {
                icon.name: "list-add"
                text: "Add"
                onTriggered: openFileDialog.open()
            }
        }

        delegate: imageListDelegate
    }

    Component {
        id: imageListDelegate
        Kirigami.AbstractCard {
            contentItem: Item {
                implicitWidth: delegateLayout.implicitWidth
                implicitHeight: delegateLayout.implicitHeight
                GridLayout {
                    id: delegateLayout
                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                    }
                    rowSpacing: Kirigami.Units.largeSpacing
                    columnSpacing: Kirigami.Units.largeSpacing
                    columns: applicationWindow().wideScreen ? 4 : 2
                    KQuickImageEditor.ImageItem {
                        image: model.image
                        Layout.fillHeight: true
                        Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                        Layout.preferredWidth: height
                    }
                    Controls.Label {
                        Layout.fillWidth: true
                        wrapMode: Text.Wrap
                        text: model.url
                    }
                    Controls.Button {
                        Layout.alignment: Qt.AlignRight
                        Layout.columnSpan: 2
                        text: i18n("Remove")
                        icon.name: "delete"
                        onClicked: backend.removeImage(index)
                    }
                }
            }
        }
    }
}
