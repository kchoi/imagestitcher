import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import QtQuick.Dialogs
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kquickimageeditor 1.0 as KQuickImageEditor

Kirigami.Page {
    title: i18nc("@title", "Result")

    actions: [
        Kirigami.Action {
            id: saveAction
            icon.name: "document-save"
            text: i18nc("@action:button", "Save Result")
            shortcut: "Ctrl+s"
            onTriggered: backend.save(saveFileDialog.selectedFile)
            enabled: saveFileDialog.selectedFile != Qt.resolvedUrl("")
        },
        Kirigami.Action {
            icon.name: "document-save-as"
            text: i18nc("@action:button", "Save As")
            onTriggered: saveFileDialog.open()
        },
        Kirigami.Action {
            icon.name: "delete"
            text: i18nc("@action:button", "Clear Result")
            onTriggered: {
                pageStack.pop()
                backend.clearStitched()
            }
        }
    ]

    FileDialog {
        id: saveFileDialog
        fileMode: FileDialog.SaveFile
        //currentFolder: StandardPaths.standardLocations(StandardPaths.PicturesLocation)[0]
        currentFolder: "file:/home/kwon-young/Documents/peoples/Papa/"
        selectedNameFilter.index: 0
        nameFilters: ["All Supported Formats (*.jpg *.bmp *.dib *.jpeg *.jpe *.jp2 *.png *.webp *.avif *.pbm *.pgm *.ppm *.pxm *.pnm *.pfm *.sr *.ras *.tiff *.tif *.exr *.hdr *.pic)",
                      "Windows bitmaps (*.bmp *.dib)",
                      "JPEG files (*.jpeg *.jpg *.jpe)",
                      "JPEG 2000 files (*.jp2)",
                      "Portable Network Graphics (*.png)",
                      "WebP (*.webp)",
                      "AVIF (*.avif)",
                      "Portable image format (*.pbm *.pgm *.ppm *.pxm *.pnm)",
                      "PFM files (*.pfm)",
                      "Sun rasters (*.sr *.ras)",
                      "TIFF files (*.tiff *.tif)",
                      "OpenEXR Image files (*.exr)",
                      "Radiance HDR (*.hdr *.pic)"]
        onAccepted: backend.save(selectedFile)
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Kirigami.InlineMessage {
            id: saveMessage
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            text: saveFileDialog.selectedFile + " saved"
            type: Kirigami.MessageType.Positive
            showCloseButton: true

            Connections {
                target: backend
                function onSaveFinished() {
                    saveMessage.visible = true
                    saveMessageTimer.restart()
                }
            }

            Timer {
                id: saveMessageTimer
                interval: 5000
                onTriggered: {
                    saveMessage.visible = false
                }
            }
        }
        Kirigami.InlineMessage {
            id: errorMessage
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            text: "Stitching failed"
            type: Kirigami.MessageType.Error
            visible: resultImage.status === Image.Error
        }
        Controls.BusyIndicator {
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            running: resultImage.status === Image.Loading
            visible: resultImage.status === Image.Loading
        }
        KQuickImageEditor.ImageItem {
            id: resultImage
            image: backend.stitched
            Layout.fillWidth: true
            Layout.fillHeight: true
            fillMode: KQuickImageEditor.ImageItem.PreserveAspectFit
        }
    }
}
