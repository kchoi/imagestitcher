#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QQuickStyle>
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include "backend.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("imagestitcher");
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));
    QCoreApplication::setApplicationName(QStringLiteral("ImageStitcher"));

    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }

    KAboutData aboutData(
        QStringLiteral("imagestitcher"),
        i18nc("@title", "ImageStitcher"),
        QStringLiteral("1.0"),
        i18n("Image stitching application"),
        KAboutLicense::GPL,
        i18n("(c) 2024"));

    aboutData.addAuthor(
        i18nc("@info:credit", "Kwon-Young Choi"),
        i18nc("@info:credit", "Developer"),
        QStringLiteral("kwon-young.choi@hotmail.fr"),
        QStringLiteral("https://kwon-young.github.io"));
    aboutData.addComponent(QStringLiteral("OpenCV"),
                           QStringLiteral("Open Source Computer Vision Library"),
                           QStringLiteral("4.9.0"),
                           QStringLiteral("https://opencv.org"),
                           KAboutLicense::byKeyword(QStringLiteral("Apache 2.0")).key());

    // Set aboutData as information about the app
    KAboutData::setApplicationData(aboutData);

    // Register a singleton that will be accessible from QML.
    qmlRegisterSingletonType(
        "org.imagestitcher.about", // How the import statement should look like
        1, 0, // Major and minor versions of the import
        "About", // The name of the QML object
        [](QQmlEngine* engine, QJSEngine *) -> QJSValue {
            // Here we retrieve our aboutData and give it to the QML engine
            // to turn it into a QML type
            return engine->toScriptValue(KAboutData::applicationData());
        }
    );

    qmlRegisterType<Backend>(
        "org.imagestitcher.backend",
        1, 0,
        "Backend");

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
