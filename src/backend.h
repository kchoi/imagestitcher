#pragma once

#include <vector>
#include <QObject>
#include <QImage>
#include <QList>
#include <QUrl>
#include <QByteArray>
#include <QAbstractListModel>
#include <KIO/StoredTransferJob>
#include <opencv2/core/mat.hpp>

class Backend : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QImage stitched READ stitched NOTIFY stitchedChanged)

private:
    cv::Mat m_stitched;
    QList<QUrl> urls;
    std::vector<cv::Mat> images;

    void uploadFinished(KJob *job);
    void downloadFinished(KJob *job);

public:
    enum Roles {
        UrlRole = Qt::UserRole,
        ImageRole
    };

    explicit Backend(QObject *parent = nullptr);
    Q_INVOKABLE void stitch();
    QImage stitched() const;
    Q_SIGNAL void stitchedChanged();
    int rowCount(const QModelIndex &) const override;
    Q_INVOKABLE QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE void addImage(const QUrl& url);
    Q_INVOKABLE void removeImage(const int index);
    Q_INVOKABLE void removeAllImages();
    Q_INVOKABLE void save(const QUrl& url);
    Q_SIGNAL void saveFinished();
    Q_INVOKABLE void clearStitched();
};
