# ImageStitcher

A very simple program to stitch microscope images.

## How to use ImageStitcher

From the welcome screen:

![welcome screen](data/welcome.png)

Select your images:

![select images](data/list.png)

And click stitch:

![stitched image](data/stitch.png)
